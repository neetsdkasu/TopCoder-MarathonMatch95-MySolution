var xhr = (function() {
	if (window.XMLHttpRequest) {
		return new XMLHttpRequest();
	}
	if (window.ActiveXObject) {
		try {
			return new ActiveXObject("Microsoft.XMLHTTP");
		} catch (_) {}
		try {
			return new ActiveXObject("Msxml2.XMLHTTP");
		} catch (_) {}
	}
	return {
		"open": function() {},
		"send": function() {}
	};
})();
function OrdName(n) {
	var mod10 = n % 10;
	var mod100 = n % 100;
	if (mod10 === 1 && mod100 !== 11) {
		return n.toString() + 'st';
	}
	if (mod10 === 2 && mod100 !== 12) {
		return n.toString() + 'nd';
	}
	if (mod10 === 3 && mod100 !== 13) {
		return n.toString() + 'rd';
	}
	return n.toString() + 'th';
}
function formatFloat(v) {
	var s = v.toString().split('.');
	if (s.length < 2) {
		return s[0] + '.00';
	}
	return s[0] + '.' + (s[1] + '00').substring(0, 2);
}
var scores = new Array();
var points = new Array();
var bests  = new Array();
var seconds = new Array();
var worsts = new Array();
var boobys = new Array();
function findSpecialScores() {
	for (var i = 0; i < 10; i++) {
		var ar = new Array();
		for (var j = 0; j < 16; j++) {
			ar[j] = scores[j][i];
		}
		ar.sort(function(a, b) { return a - b; });
		bests[i] = ar[0];
		worsts[i] = ar[15];
		for (var j = 1; j < 16; j++) {
			if (ar[j] === ar[0]) { continue; }
			seconds[i] = ar[j];
			break;
		}
		for (var j = 14; j >= 0; j--) {
			if (ar[j] === ar[15]) { continue; }
			boobys[i] = ar[j];
			break;
		}
	}
}
function showRanking(tp) {
	var rk = new Array();
	if (tp < 0) {
		for (var j = 1; j <= 16; j++) {
			rk[j-1] = [j];
		}
	} else if (tp === 0) { 
		for (var j = 1; j <= 16; j++) {
			rk[j-1] = [j, points[j-1]];
		}
		rk.sort(function(a, b) {
			return b[1] - a[1];
		});
	} else {
		for (var j = 1; j <= 16; j++) {
			rk[j-1] = [j, scores[j-1][tp-1]];
		}
		rk.sort(function(a, b) {
			return a[1] - b[1];
		});
	}
	for (var r = 1; r <= 16; r++) {
		var j = rk[r-1][0];
		document.getElementById('rbsb' + r.toString())
			.innerHTML = OrdName(j);
		document.getElementById('rbsc' + r.toString())
			.innerHTML = formatFloat(points[j-1]);
		for (var i = 1; i <= 10; i++) {
			var id = 'rbex' + r.toString() + 'no' + i.toString();
			var e = document.getElementById(id);
			var sc = scores[j-1][i-1];
			if (sc === bests[i-1]) {
				e.className = 'bestscore';
			} else if (sc === seconds[i-1]) {
				e.className = 'betterscore';
			} else if (sc === boobys[i-1]) {
				e.className = 'wrongscore';
			} else if (sc === worsts[i-1]) {
				e.className = 'worstscore';
			} else {
				e.className = '';
			}
			e.innerHTML = sc.toString();		}
	}
}
function showScores(i) {
	for (var j = 1; j <= 16; j++) {
		var id = 'score' + j.toString();
		var e = document.getElementById(id);
		var sc = scores[j-1][i-1];
		if (sc === bests[i-1]) {
			e.className = 'bestscore';
		} else if (sc === seconds[i-1]) {
			e.className = 'betterscore';
		} else if (sc === boobys[i-1]) {
			e.className = 'wrongscore';
		} else if (sc === worsts[i-1]) {
			e.className = 'worstscore';
		} else {
			e.className = '';
		}
		e.innerHTML = sc.toString();
	}
}
function changeImages() {
	var sel = document.getElementById('selexample').value;
	for (var j = 1; j <= 16; j++) {
		var id = 'img' + j.toString();
		var e = document.getElementById(id);
		e.className = 'example' + sel.toString();
		e.src = './submit' + j.toString() + '/' + sel.toString() + '.png';
	}
	showScores(sel);
}
function openBigimg(src) {
	var img = document.getElementById('bigimg');
	if (img.className === 'bigimgon') { return; }
	img.src = src;
	img.className = 'bigimgon';
}
function closeBigimg() {
	document.getElementById('bigimg').className = 'bigimgoff';
}
function buildImgTable() {
	var tb = document.createElement('table');
	tb.border = 1;
	for (var i = 0; i < 4; i++) {
		var tr1 = document.createElement('tr');
		var tr2 = document.createElement('tr');
		for (var j = 0; j < 4; j++) {
			var n = i * 4 + j + 1;
			var nstr = OrdName(n);
			var td1 = document.createElement('td');
			var td2 = document.createElement('td');
			var td3 = document.createElement('td');
			var sp1 = document.createElement('span');
			var sp2 = document.createElement('span');
			var img = document.createElement('img');
			td1.className = 'resulttitle';
			sp1.innerHTML = nstr;;
			td1.appendChild(sp1);
			tr1.appendChild(td1);
			td2.className = 'score';
			sp2.id = 'score' + n.toString();
			sp2.innerHTML = '0.0';
			td2.appendChild(sp2);
			tr1.appendChild(td2);
			td3.colSpan = 2;
			img.id = 'img' + n.toString();
			img.className = 'example1';
			img.src = './submit' + n.toString() + '/1.png';
			img.onclick = (function(im) {
				return function() {openBigimg(im.src); };
			})(img);
			td3.appendChild(img);
			tr2.appendChild(td3);
		}
		tb.appendChild(tr1);
		tb.appendChild(tr2);
	}
	document.getElementById('imgtable').appendChild(tb);
}
function buildSelector() {
	var sel = document.createElement('select');
	sel.id = 'selexample';
	for (var i = 1; i <= 10; i++) {
		var op = document.createElement('option');
		op.value = i.toString();
		if (i === 1) { op.selected = true; }
		op.appendChild(
			document.createTextNode('Example No.' + i.toString())
		);
		sel.appendChild(op);
	}
	var btn = document.createElement('button');
	btn.onclick = changeImages;
	btn.appendChild(
		document.createTextNode('submit')
	);
	var e = document.getElementById('selector');
	e.appendChild(sel);
	e.appendChild(btn);
}
function calcPoints() {
	for (var j = 1; j <= 16; j++) {
		var pt = 0.0;
		for (var i = 0; i < 10; i++) {
			var sc = scores[j-1][i];
			for (var k = 1; k <= 16; k++) {
				if (j === k) { continue; }
				if (sc < scores[k-1][i]) { pt += 1.0; }
				if (sc === scores[k-1][i]) { pt += 0.5; }
			}
		}
		points[j-1] = (pt / 15.0) * 1000000.0 / 10.0;
	}
}
function buildRankingBoard() {
	var tb = document.createElement('table');
	tb.border = 1;
	var thr = document.createElement('tr');
	var th = document.createElement('th');
	th.innerHTML = 'rank'; thr.appendChild(th);
	th = document.createElement('th');
	th.onclick = function() { showRanking(-1); };
	th.innerHTML = 'submit'; thr.appendChild(th);
	th = document.createElement('th');
	th.onclick = function() { showRanking(0); };
	th.innerHTML = 'score'; thr.appendChild(th);
	for (var i = 1; i <= 10; i++) {
		th = document.createElement('th');
		th.onclick = (function(tp) {
			return function() { showRanking(tp); };
		})(i);
		th.innerHTML = 'No.' + i.toString(); thr.appendChild(th);
	}
	tb.appendChild(thr);
	for (var j = 1; j <= 16; j++) {
		var tr = document.createElement('tr');
		var rk = document.createElement('td');
		var sb = document.createElement('td');
		var sc = document.createElement('td');
		var sbsp = document.createElement('span');
		var scsp = document.createElement('span');
		rk.style.textAlign = 'center';
		rk.innerHTML = j.toString();
		tr.appendChild(rk);
		sbsp.id = 'rbsb' + j.toString();
		sbsp.style.fontWeight = 'bold';
		sb.appendChild(sbsp);
		sb.style.textAlign = 'center';
		tr.appendChild(sb);
		scsp.id = 'rbsc' + j.toString();
		sc.appendChild(scsp);
		sc.className = 'score';
		tr.appendChild(sc);
		for (var i = 1; i <= 10; i++) {
			var ex = document.createElement('td');
			var exsp = document.createElement('span');
			exsp.id = 'rbex' + j.toString() + 'no' + i.toString();
			ex.appendChild(exsp);
			ex.className = 'score';
			tr.appendChild(ex);
		}
		tb.appendChild(tr);
	}
	document.getElementById('rankboard').appendChild(tb);
}
function build() {
	document.getElementById('bigimg').onclick = closeBigimg;
	calcPoints();
	findSpecialScores();
	buildSelector();
	buildImgTable();
	buildRankingBoard();
	showScores(1);			
	showRanking(0);
}
function loadScore(i) {
	if (i > 16) {
		build();
		return;
	}
	xhr.onreadystatechange = function() {
	if (xhr.readyState !== 4) { return; }
	if (xhr.status !== 200) { return; }
		var tx = xhr.responseText.replace(/[\r\n]+/, '\n').split('\n');
		var ar = new Array();
		var j = 0;
		for (var k in tx) {
			var ln = tx[k].split(' ');
			if (ln[0] !== 'Score') { continue; }
			ar[j] = parseFloat(ln[2]);
			j++;
		}
		scores[i-1] = ar;
		loadScore(i+1);
	};
	xhr.open('GET', './submit'+i.toString()+'/score.txt', true);
	if (xhr.setRequestHeader) {
		xhr.setRequestHeader('Content-type', 'text/plain');
	}
	xhr.overrideMimeType = 'text/plain';
	xhr.send(null);
}
(function(f) {
	if (window.attachEvent) {
		window.attachEvent('onload', f);
	} else if (window.addEventListener) {
		window.addEventListener('load', f);
	}
})(function() {
	loadScore(1);
});