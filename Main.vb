Option Explicit On
Option Strict   On
Option Compare  Binary
Option Infer    On
Imports Console = System.Console

Public Module Main

	Public Sub Main()
		Try
			Dim H As Integer = CInt(Console.ReadLine())
			Dim S As Integer = CInt(Console.ReadLine())
			Dim pixels(S - 1) As Integer
			For i As Integer = 0 To UBound(pixels)
				pixels(i) = CInt(Console.ReadLine())
			Next i
			Dim N As Integer = CInt(Console.ReadLine())
			
			Dim cm As New CirclesMix()
			Dim ret() As Integer = cm.drawImage(H, pixels, N)

			Console.WriteLine(ret.Length)
			For Each r As Integer In ret
				Console.WriteLine(r)
			Next r
			Console.Out.Flush()
			
		Catch Ex As Exception
			Console.Error.WriteLine(ex)
		End Try
	End Sub

End Module