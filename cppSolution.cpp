#include <algorithm>
#include <vector>
#include <cmath>
#include <iostream>

#ifdef _MSC_VER
#include <chrono>
inline auto get_time() {
	return std::chrono::system_clock::now();
}
inline auto to_msec(int t) {
	return std::chrono::milliseconds(t);
}
#else
const double ticks_per_sec = 2800000000;
inline double get_time() {
	uint32_t lo, hi;
	asm volatile ("rdtsc" : "=a" (lo), "=d" (hi));
	return (((uint64_t)hi << 32) | lo) / ticks_per_sec;
}
inline double to_msec(int t) {
	return double(t) / 1000.0;
}
#endif

using std::fill;
using std::swap;
using std::vector;
using std::abs;
using std::max;
using std::min;

typedef long long int64;
typedef unsigned long long uint64;

uint64 XSFT(88172645463325252ULL);
inline uint64 rnd() {
	XSFT ^= XSFT << 13; XSFT ^= XSFT >> 17;
	return (XSFT ^= XSFT << 5);
}
inline uint64 randNext(uint64 upper) { return rnd() % upper; }
inline uint64 randNext(uint64 lower, uint64 upper)
{ return rnd() % (upper - lower) + lower; }

class CirclesMix {
	int picsH, picsW;
	bool *flags[1001];
	vector<int>
		Rpics, Gpics, Bpics,
		Rcur, Gcur, Bcur,
		Rret, Gret, Bret;
	int64 curSc;
	void drawCircle(int cx, int cy, int rad, vector<int> &ret) {
		int wMin(max(cx - rad, 0) - cx);
		int wMax(min(cx + rad, picsW - 1) - cx);
		int hMin(max(cy - rad, 0) - cy);
		int hMax(min(cy + rad, picsH - 1) - cy);
		int dia(rad + rad + 1);
		bool *fg(flags[rad]);
		int Rp(0), Gp(0), Bp(0), Cnt(0);
		int64 oldRsc(0), oldGsc(0), oldBsc(0);
		for (int dy(hMin), y((cy + hMin - 1) * picsW), fy((hMin + rad - 1) * dia)
				; dy <= hMax; dy++) {
			y += picsW;
			fy += dia;
			for (int dx(wMin); dx <= wMax; dx++) {
				if (fg[fy + dx + rad]) { continue; }
				int i(y + cx + dx);
				Rp += Rpics[i];
				Gp += Gpics[i];
				Bp += Bpics[i];
				oldRsc += abs(Rcur[i] - Rpics[i]);
				oldGsc += abs(Gcur[i] - Gpics[i]);
				oldBsc += abs(Bcur[i] - Bpics[i]);
				Cnt++;
			}
		}
		if (Cnt == 0) { return; }
		int64 oldSc(oldRsc + oldGsc + oldBsc);
		Rp /= Cnt;
		Gp /= Cnt;
		Bp /= Cnt;
		int Rp05(Rp / 2), Rp15(min(0xFF, Rp * 3 / 2));
		int Gp05(Gp / 2), Gp15(min(0xFF, Gp * 3 / 2));
		int Bp05(Bp / 2), Bp15(min(0xFF, Bp * 3 / 2));
		int64 newRsc(0), newR05sc(0), newR15sc(0);
		int64 newGsc(0), newG05sc(0), newG15sc(0);
		int64 newBsc(0), newB05sc(0), newB15sc(0);
		for (int dy(hMin), y((cy + hMin - 1) * picsW), fy((hMin + rad - 1) * dia)
				; dy <= hMax; dy++) {
			y += picsW;
			fy += dia;
			for (int dx(wMin); dx <= wMax; dx++) {
				if (fg[fy + dx + rad]) { continue; }
				int i(y + cx + dx);
				int rc(Rcur[i]), ro(Rpics[i]);
				newRsc   += abs((rc + Rp)   / 2 - ro);
				newR05sc += abs((rc + Rp05) / 2 - ro);
				newR15sc += abs((rc + Rp15) / 2 - ro);
				int gc(Gcur[i]), go(Gpics[i]);
				newGsc   += abs((gc + Gp)   / 2 - go);
				newG05sc += abs((gc + Gp05) / 2 - go);
				newG15sc += abs((gc + Gp15) / 2 - go);
				int bc(Bcur[i]), bo(Bpics[i]);
				newBsc   += abs((bc + Bp)   / 2 - bo);
				newB05sc += abs((bc + Bp05) / 2 - bo);
				newB15sc += abs((bc + Bp15) / 2 - bo);
			}
		}
		if (newR05sc < newRsc) { newRsc = newR05sc; Rp = Rp05; }
		if (newR15sc < newRsc) { newRsc = newR15sc; Rp = Rp15; }
		if (newG05sc < newGsc) { newGsc = newG05sc; Gp = Gp05; }
		if (newG15sc < newGsc) { newGsc = newG15sc; Gp = Gp15; }
		if (newB05sc < newBsc) { newBsc = newB05sc; Bp = Bp05; }
		if (newB15sc < newBsc) { newBsc = newB15sc; Bp = Bp15; }
		int64 newSc(newRsc + newGsc + newBsc);
		if (newSc >= oldSc) { return; }
		for (int dy(hMin), y((cy + hMin - 1) * picsW), fy((hMin + rad - 1) * dia)
				; dy <= hMax; dy++) {
			y += picsW;
			fy += dia;
			for (int dx(wMin); dx <= wMax; dx++) {
				if (fg[fy + dx + rad]) { continue; }
				int i(y + cx + dx);
				Rcur[i] = (Rcur[i] + Rp) / 2;
				Gcur[i] = (Gcur[i] + Gp) / 2;
				Bcur[i] = (Bcur[i] + Bp) / 2;
			}
		}
		curSc += newSc - oldSc;
		ret.push_back(cy);
		ret.push_back(cx);
		ret.push_back(rad);
		ret.push_back((Rp << 16) | (Gp << 8) | Bp);
	}
public:
	CirclesMix() {
		for (int i(0); i < 1001; i++) { flags[i] = NULL; }
	}
	~CirclesMix() {
		for (int i(0); i < 1001; i++) { delete [] flags[i]; }
	}
    vector<int> drawImage(int H, vector<int> pixels, int N) {
		auto time0 = get_time() + to_msec(19800);
		auto time9 = time0 - to_msec(6000);
		
		int W(pixels.size() / H);
		const int DS(1);
		
		picsH = (H + DS - 1) / DS;
		picsW = (W + DS - 1) / DS;
		int picCount(picsH * picsW);
		int maxRad(max(picsH, picsW));
		int minRad(min(100, maxRad / 2));
		
		for (int i(1); i <= maxRad; i++) {
			int d(i + i + 1);
			int r2(i * i);
			auto fg = new bool[d * d];
			for (int dy(-i); dy <= i; dy++) {
				int y(dy + i);
				int inr = r2 - dy * dy;
				for (int dx(-i); dx <= i; dx++) {
					int x(dx + i);
					fg[y * d + x] = dx * dx > inr;
				}
			}
			flags[i] = fg;
		}
		
		Rpics.resize(picCount, 0);
		Gpics.resize(picCount, 0);
		Bpics.resize(picCount, 0);
		int64 startSc(0);
		for (int y(0); y < H; y++) {
			int iy = (y / DS) * picsW;
			for (int x(0); x < W; x++) {
				int p(pixels[y * W + x]);
				int i(iy + (x / DS));
				Rpics[i] += p >> 16;
				Gpics[i] += (p >> 8) & 0xFF;
				Bpics[i] += p & 0xFF;
			}
		}
		int remW(W % DS), remH(H % DS);
		for (int y(0); y < picsH; y++) {
			int dh((y < picsH - 1 || remH == 0) ? DS : remH);
			for (int x(0); x < picsW; x++) {
				int i(y * picsW + x);
				int dw((x < picsW - 1 || remW == 0) ? DS : remW);
				int dd(dh * dw);
				Rpics[i] /= dd;
				Gpics[i] /= dd;
				Bpics[i] /= dd;
				startSc += Rpics[i] + Gpics[i] + Bpics[i];
			}
		}
		
		vector<int> ret;
		int64 retSc(0x7FFFFFFFFFFFFFFLL);
		
		vector<int> tmp;
		int64 tmpSc(startSc);
		vector<int> Rtmp(picCount, 0);
		vector<int> Gtmp(picCount, 0);
		vector<int> Btmp(picCount, 0);
		
		vector<int> foo;
		
		Rcur.resize(picCount);
		Gcur.resize(picCount);
		Bcur.resize(picCount);
		
		for (int i(0); i < 10; i++) {
			fill(Rcur.begin(), Rcur.end(), 0);
			fill(Gcur.begin(), Gcur.end(), 0);
			fill(Bcur.begin(), Bcur.end(), 0);
			curSc = startSc;
			foo.clear();
			
			for (int j(0); j < 8; j++) {
				int cx(randNext(picsW + picsW) - picsW / 2);
				int cy(randNext(picsH + picsH) - picsH / 2);
				int rad(randNext(minRad, maxRad));
				drawCircle(cx, cy, rad, foo);
			}
			
			if (curSc < tmpSc) {
				tmpSc = curSc;
				swap(Rtmp, Rcur);
				swap(Gtmp, Gcur);
				swap(Btmp, Bcur);
				swap(foo, tmp);
			}
		}
		
		for (int k(0); k < 5; k++) {
			swap(Rtmp, Rret);
			swap(Gtmp, Gret);
			swap(Btmp, Bret);
			retSc = tmpSc;
			ret.insert(ret.end(), tmp.begin(), tmp.end());
			
			fill(Rtmp.begin(), Rtmp.end(), 0);
			fill(Gtmp.begin(), Gtmp.end(), 0);
			fill(Btmp.begin(), Btmp.end(), 0);
			tmp.clear();
			
			for (int i(0); i < 10; i++) {
				Rcur.clear(); Rcur.insert(Rcur.end(), Rret.begin(), Rret.end());
				Gcur.clear(); Gcur.insert(Gcur.end(), Gret.begin(), Gret.end());
				Bcur.clear(); Bcur.insert(Bcur.end(), Bret.begin(), Bret.end());
				curSc = retSc;
				foo.clear();
				
				for (int j(0); j+j+j+j+j+j+j < N; j++) {
					int cx(randNext(picsW));
					int cy(randNext(picsH));
					int rad(randNext(50, 100));
					drawCircle(cx, cy, rad, foo);
					if (!(j&3)) {
						auto time1 = get_time();
						if (time1 > time9) { goto nextstep; }
					}
				}
				
				if (curSc < tmpSc) {
					tmpSc = curSc;
					swap(Rtmp, Rcur);
					swap(Gtmp, Gcur);
					swap(Btmp, Bcur);
					swap(foo, tmp);
				}
			}
		}
nextstep:
		
		swap(Rtmp, Rret);
		swap(Gtmp, Gret);
		swap(Btmp, Bret);
		retSc = tmpSc;
		ret.insert(ret.end(), tmp.begin(), tmp.end());

		fill(Rtmp.begin(), Rtmp.end(), 0);
		fill(Gtmp.begin(), Gtmp.end(), 0);
		fill(Btmp.begin(), Btmp.end(), 0);
		tmp.clear();
		int remN(N - ret.size() / 4);
		
		for (int i(0); i < 10000; i++) {
			Rcur.clear(); Rcur.insert(Rcur.end(), Rret.begin(), Rret.end());
			Gcur.clear(); Gcur.insert(Gcur.end(), Gret.begin(), Gret.end());
			Bcur.clear(); Bcur.insert(Bcur.end(), Bret.begin(), Bret.end());
			curSc = retSc;
			foo.clear();
			
			int j(0);
			do {
				int cx(randNext(picsW));
				int cy(randNext(picsH));
				int rad(randNext(20, 60));
				drawCircle(cx, cy, rad, foo);
				if (!(j&3)) {
					auto time1 = get_time();
					if (time1 > time0) {
						std::cerr << "i:"         << i
								  << " fooCount:" << (foo.size() / 4)
								  << std::endl;
						goto endloop;
					}
				}
				j++;
			} while (foo.size() < 4 * remN);
			
			if (curSc < tmpSc) {
				tmpSc = curSc;
				swap(Rtmp, Rcur);
				swap(Gtmp, Gcur);
				swap(Btmp, Bcur);
				swap(foo, tmp);
			}
		}
endloop:
		
		if (tmp.size() < 0) {
			swap(tmp, foo);
			tmpSc = curSc;
		}
		
		retSc = tmpSc;
		ret.insert(ret.end(), tmp.begin(), tmp.end());
		
		std::cerr << "LngSc:" << retSc
		          << " N:"    << N
				  << " retN:" << (ret.size() / 4)
				  << std::endl;
		
		for (int i(0); i < ret.size(); i++) {
			if (i % 4 == 3) { continue; }
			ret[i] *= DS;
		}

        return ret;
    }
};