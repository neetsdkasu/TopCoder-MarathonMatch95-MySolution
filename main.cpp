#include "cppSolution.cpp"

#include <iostream>
#include <vector>

using std::cin;
using std::cout;
using std::endl;
using std::vector;

int main() {
	int H; cin >> H;
	int S; cin >> S;
	vector<int> pixels(S);
	for (int i = 0; i < pixels.size(); i++) {
		cin >> pixels[i];
	}
	int N; cin >> N;

	CirclesMix cm;
	vector<int> ret = cm.drawImage(H, pixels, N);
	
	cout << ret.size() << endl;
	for (int i = 0; i < ret.size(); i++) {
		cout << ret[i] << endl;
	}
	cout.flush();
}
