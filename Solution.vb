Option Explicit On
Option Strict   On
Option Compare  Binary
Option Infer    On

Imports System
Imports System.Collections.Generic

Public Class CirclesMix
	Dim rand As New Random(19831983)
	
	Dim picsH As Integer
	Dim picsW As Integer

	Dim flags(1000)(,) As Boolean
	
	Dim Rpics() As Integer
	Dim Gpics() As Integer
	Dim Bpics() As Integer
	
	Dim Rcur() As Integer
	Dim Gcur() As Integer
	Dim Bcur() As Integer
	Dim curSc As Long

	Dim Rret() As Integer
	Dim Gret() As Integer
	Dim Bret() As Integer
	
	Private Sub drawCircle(cx As Integer, cy As Integer, rad As Integer, ret As List(Of Integer))
		Dim wMin As Integer = Math.Max(cx - rad, 0) - cx
		Dim wMax As Integer = Math.Min(cx + rad, picsW - 1) - cx
		Dim hMin As Integer = Math.Max(cy - rad, 0) - cy
		Dim hMax As Integer = Math.Min(cy + rad, picsH - 1) - cy
		Dim fg(,) As Boolean = flags(rad)
		Dim Rp As Integer = 0
		Dim Gp As Integer = 0
		Dim Bp As Integer = 0
		Dim Cnt As Integer = 0
		Dim oldRsc As Long = 0
		Dim oldGsc As Long = 0
		Dim oldBsc As Long = 0
		For dy As Integer = hMin To hMax
			Dim y As Integer = (cy + dy) * picsW
			For dx As Integer = wMin To wMax
				If fg(dy + rad, dx + rad) Then Continue For
				Dim i As Integer = y + cx + dx
				Rp += Rpics(i)
				Gp += Gpics(i)
				Bp += Bpics(i)
				oldRsc += Math.Abs(Rcur(i) - Rpics(i))
				oldGsc += Math.Abs(Gcur(i) - Gpics(i))
				oldBsc += Math.Abs(Bcur(i) - Bpics(i))
				Cnt += 1
			Next dx
		Next dy
		If Cnt = 0 Then
			Exit Sub
		End If
		Dim oldSc As Long = oldRsc + oldGsc + oldBsc
		Rp \= Cnt
		Gp \= Cnt
		Bp \= Cnt
		Dim Rp05 As Integer = Rp \ 2
		Dim Gp05 As Integer = Gp \ 2
		Dim Bp05 As Integer = Bp \ 2
		Dim Rp15 As Integer = Math.Min(&HFF, Rp * 3 \ 2)
		Dim Gp15 As Integer = Math.Min(&HFF, Gp * 3 \ 2)
		Dim Bp15 As Integer = Math.Min(&HFF, Bp * 3 \ 2)
		Dim newRsc As Long = 0
		Dim newGsc As Long = 0
		Dim newBsc As Long = 0
		Dim newR05sc As Long = 0
		Dim newG05sc As Long = 0
		Dim newB05sc As Long = 0
		Dim newR15sc As Long = 0
		Dim newG15sc As Long = 0
		Dim newB15sc As Long = 0
		For dy As Integer = hMin To hMax
			Dim y As Integer = (cy + dy) * picsW
			For dx As Integer = wMin To wMax
				If fg(dy + rad, dx + rad) Then Continue For
				Dim i As Integer = y + cx + dx
				Dim rc As Integer = Rcur(i)
				Dim ro As Integer = Rpics(i)
				newRsc += Math.Abs((rc + Rp) \ 2 - ro)
				newR05sc += Math.Abs((rc + Rp05) \ 2 - ro)
				newR15sc += Math.Abs((rc + Rp15) \ 2 - ro)
				Dim gc As Integer = Gcur(i)
				Dim go As Integer = Gpics(i)
				newGsc += Math.Abs((gc + Gp) \ 2 - go)
				newG05sc += Math.Abs((gc + Gp05) \ 2 - go)
				newG15sc += Math.Abs((gc + Gp15) \ 2 - go)
				Dim bc As Integer = Bcur(i)
				Dim bo As Integer = Bpics(i)
				newBsc += Math.Abs((bc + Bp) \ 2 - bo)
				newB05sc += Math.Abs((bc + Bp05) \ 2 - bo)
				newB15sc += Math.Abs((bc + Bp15) \ 2 - bo)
			Next dx
		Next dy
		If newR05sc < newRsc Then
			newRsc = newR05sc: Rp = Rp05
		End If
		If newR15sc < newRsc Then
			newRsc = newR15sc: Rp = Rp15
		End If
		If newg05sc < newGsc Then
			newGsc = newG05sc: Gp = Gp05
		End If
		If newG15sc < newGsc Then
			newGsc = newG15sc: Gp = Gp15
		End If
		If newB05sc < newBsc Then
			newBsc = newB05sc: Bp = Bp05
		End If
		If newB15sc < newBsc Then
			newBsc = newB15sc: Bp = Bp15
		End If
		Dim newSc As Long = newRsc + newGsc + newBsc
		If newSc >= oldSc Then Exit Sub
		For dy As Integer = hMin To hMax
			Dim y As Integer = (cy + dy) * picsW
			For dx As Integer = wMin To wMax
				If fg(dy + rad, dx + rad) Then Continue For
				Dim i As Integer = y + cx + dx
				Rcur(i) = (Rcur(i) + Rp) \ 2
				Gcur(i) = (Gcur(i) + Gp) \ 2
				Bcur(i) = (Bcur(i) + Bp) \ 2
			Next dx
		Next dy
		curSc += newSc - oldSc
		ret.Add(cy)
		ret.Add(cx)
		ret.Add(rad)
		ret.Add((Rp << 16) Or (Gp << 8) Or Bp)
	End Sub
	
	Public Function drawImage(H As Integer, pixels() As Integer, N As Integer) As Integer()
		Dim time0 As Integer = Environment.TickCount + 19800
		Dim time9 As Integer = time0 - 8000
		Dim time1 As Integer
		
		Dim W As Integer = pixels.Length \ H
		
		Const DS As Integer = 2
		
		picsH = (H + DS - 1) \ DS
		picsW = (W + DS - 1) \ DS
		Dim picCount As Integer = picsH * picsW - 1
		Dim maxRad As Integer = Math.Max(picsH, picsW)
		Dim minRad As Integer = Math.Min(100, maxRad \ 2)
		
		For i As Integer = 1 To maxRad
			ReDim flags(i)(i + i, i + i)
			For dy As Integer = -i To i
				Dim y As Integer = dy + i
				For dx As Integer = -i To i
					Dim x As Integer = dx + i
					flags(i)(y, x) = dx * dx + dy * dy > i * i
				Next dx
			Next dy
		Next i
		
		ReDim Rpics(picCount)
		ReDim Gpics(picCount)
		ReDim Bpics(picCount)
		Dim startSc As Long = 0
		For y As Integer = 0 To H - 1
			For x As Integer = 0 To W - 1
				Dim p As Integer = pixels(y * W + x)
				Dim i As Integer = (y \ DS) * picsW + (x \ DS)
				Rpics(i) += p >> 16
				Gpics(i) += (p >> 8) And &HFF
				Bpics(i) += p And &HFF
			Next x
		Next y
		Dim remW As Integer = W Mod DS
		Dim remH As Integer = H Mod DS
		For y As Integer = 0 To picsH - 1
			Dim dh As Integer = If(y < picsH - 1 OrElse remH = 0, DS, remH)
			For x As Integer = 0 To picsW - 1
				Dim i As Integer = y * picsW + x
				Dim dw As Integer = If(x < picsW - 1 OrElse remW = 0, DS, remW)
				Dim dd As Integer = dw * dh
				Rpics(i) \= dd
				Gpics(i) \= dd
				Bpics(i) \= dd
				startSc += Rpics(i) + Gpics(i) + Bpics(i)
			Next x
		Next y
		
		Dim ret As New List(Of Integer)()
		Dim retSc As Long = Long.MaxValue

		Dim tmp As New List(Of Integer)()
		Dim tmpSc As Long = startSc
		Dim Rtmp(picCount) As Integer
		Dim Gtmp(picCount) As Integer
		Dim Btmp(picCount) As Integer
		ReDim Rcur(picCount)
		ReDim Gcur(picCount)
		ReDim Bcur(picCount)
		ReDim Rret(picCount)
		ReDim Gret(picCount)
		ReDim Bret(picCount)
		
		Dim swapArr() As Integer
		Dim swapLst As List(Of Integer)
		
		Dim foo As New List(Of Integer)()
		
		For i As Integer = 1 To 10
			Array.Clear(Rcur, 0, Rcur.Length)
			Array.Clear(Gcur, 0, Gcur.Length)
			Array.Clear(Bcur, 0, Bcur.Length)
			curSc = startSc
			foo.Clear()
			
			For j As Integer = 1 To 8
				Dim cx As Integer = rand.Next(picsW + picsW) - picsW \ 2
				Dim cy As Integer = rand.Next(picsH + picsH) - picsH \ 2
				Dim rad As Integer = rand.Next(minRad, MaxRad)
				drawCircle(cx, cy, rad, foo)
			Next j
			
			If curSc < tmpSc Then
				tmpSc = curSc
				swapArr = Rtmp: Rtmp = Rcur: Rcur = swapArr
				swapArr = Gtmp: Gtmp = Gcur: Gcur = swapArr
				swapArr = Btmp: Btmp = Bcur: Bcur = swapArr
				swapLst = tmp: tmp = foo: foo = swapLst
			End If
		Next i
		
		Dim nn As Integer = (N + 8) \ 9
		Do
			For k As Integer = 1 To 2
				swapArr = Rret: Rret = Rtmp: Rtmp = swapArr
				swapArr = Gret: Gret = Gtmp: Gtmp = swapArr
				swapArr = Bret: Bret = Btmp: Btmp = swapArr
				retSc = tmpSc
				ret.AddRange(tmp)
				
				Array.Clear(Rtmp, 0, Rtmp.Length)
				Array.Clear(Gtmp, 0, Gtmp.Length)
				Array.Clear(Btmp, 0, Btmp.Length)
				tmp.Clear()

				For i As Integer = 1 To 10
					Array.Copy(Rret, Rcur, Rcur.Length)
					Array.Copy(Gret, Gcur, Gcur.Length)
					Array.Copy(Bret, Bcur, Bcur.Length)
					curSc = retSc
					foo.Clear()

					For j As Integer = 1 To nn
						Dim cx As Integer = rand.Next(picsW)
						Dim cy As Integer = rand.Next(picsH)
						Dim rad As Integer = rand.Next(40, 100)
						drawCircle(cx, cy, rad, foo)
						time1 = Environment.TickCount
						If time1 >= time9 Then
							Exit Do
						End If
					Next j

					If curSc < tmpSc Then
						tmpSc = curSc
						swapArr = Rtmp: Rtmp = Rcur: Rcur = swapArr
						swapArr = Gtmp: Gtmp = Gcur: Gcur = swapArr
						swapArr = Btmp: Btmp = Bcur: Bcur = swapArr
						swapLst = tmp: tmp = foo: foo = swapLst
					End If
				Next i
			Next k
			
			For k As Integer = 1 To 3
				swapArr = Rret: Rret = Rtmp: Rtmp = swapArr
				swapArr = Gret: Gret = Gtmp: Gtmp = swapArr
				swapArr = Bret: Bret = Btmp: Btmp = swapArr
				retSc = tmpSc
				ret.AddRange(tmp)
				
				Array.Clear(Rtmp, 0, Rtmp.Length)
				Array.Clear(Gtmp, 0, Gtmp.Length)
				Array.Clear(Btmp, 0, Btmp.Length)
				tmp.Clear()

				For i As Integer = 1 To 10
					Array.Copy(Rret, Rcur, Rcur.Length)
					Array.Copy(Gret, Gcur, Gcur.Length)
					Array.Copy(Bret, Bcur, Bcur.Length)
					curSc = retSc
					foo.Clear()

					For j As Integer = 1 To nn
						Dim cx As Integer = rand.Next(picsW)
						Dim cy As Integer = rand.Next(picsH)
						Dim rad As Integer = rand.Next(20, 80)
						drawCircle(cx, cy, rad, foo)
						time1 = Environment.TickCount
						If time1 >= time9 Then
							Exit Do
						End If
					Next j

					If curSc < tmpSc Then
						tmpSc = curSc
						swapArr = Rtmp: Rtmp = Rcur: Rcur = swapArr
						swapArr = Gtmp: Gtmp = Gcur: Gcur = swapArr
						swapArr = Btmp: Btmp = Bcur: Bcur = swapArr
						swapLst = tmp: tmp = foo: foo = swapLst
					End If
				Next i
			Next k
			Exit Do
		Loop

		swapArr = Rret: Rret = Rtmp: Rtmp = swapArr
		swapArr = Gret: Gret = Gtmp: Gtmp = swapArr
		swapArr = Bret: Bret = Btmp: Btmp = swapArr
		retSc = tmpSc
		ret.AddRange(tmp)
		
		Array.Clear(Rtmp, 0, Rtmp.Length)
		Array.Clear(Gtmp, 0, Gtmp.Length)
		Array.Clear(Btmp, 0, Btmp.Length)
		tmp.Clear()
		Dim remN As Integer = N - ret.Count \ 4

		For i As Integer = 1 To 1000
			Array.Copy(Rret, Rcur, Rcur.Length)
			Array.Copy(Gret, Gcur, Gcur.Length)
			Array.Copy(Bret, Bcur, Bcur.Length)
			curSc = retSc
			foo.Clear()
			Do
				Dim cx As Integer = rand.Next(picsW)
				Dim cy As Integer = rand.Next(picsH)
				Dim rad As Integer = rand.Next(10, 70)
				drawCircle(cx, cy, rad, foo)
				time1 = Environment.TickCount
				If time1 >= time0 Then
					Console.Error.WriteLine("i:{0} fooCount:{1}", i, foo.Count \ 4)
					Exit For
				End If
			Loop While foo.Count < 4 * remN
				
			If curSc < tmpSc Then
				tmpSc = curSc
				swapArr = Rtmp: Rtmp = Rcur: Rcur = swapArr
				swapArr = Gtmp: Gtmp = Gcur: Gcur = swapArr
				swapArr = Btmp: Btmp = Bcur: Bcur = swapArr
				swapLst = tmp: tmp = foo: foo = swapLst
			End If
			
			Array.Copy(Rret, Rcur, Rcur.Length)
			Array.Copy(Gret, Gcur, Gcur.Length)
			Array.Copy(Bret, Bcur, Bcur.Length)
			curSc = retSc
			foo.Clear()
			Do
				Dim cx As Integer = rand.Next(picsW)
				Dim cy As Integer = rand.Next(picsH)
				Dim rad As Integer = rand.Next(5, 60)
				drawCircle(cx, cy, rad, foo)
				time1 = Environment.TickCount
				If time1 >= time0 Then
					Console.Error.WriteLine("i:{0} fooCount:{1}", i, foo.Count \ 4)
					Exit For
				End If
			Loop While foo.Count < 4 * remN
				
			If curSc < tmpSc Then
				tmpSc = curSc
				swapArr = Rtmp: Rtmp = Rcur: Rcur = swapArr
				swapArr = Gtmp: Gtmp = Gcur: Gcur = swapArr
				swapArr = Btmp: Btmp = Bcur: Bcur = swapArr
				swapLst = tmp: tmp = foo: foo = swapLst
			End If

		Next i
		
		If tmp.Count = 0 Then
			tmp = foo
			tmpSc = curSc
		End If
		
		retSc = tmpSc
		ret.AddRange(tmp)
		
		Console.Error.WriteLine("LngSc:{0} N:{1} retN:{2}", _
			retSc, N, ret.Count \ 4)
		
		For i As Integer = 0 To ret.Count - 1
			If i Mod 4 = 3 Then Continue For
			ret(i) *= DS
		Next i
		
		drawImage = ret.ToArray()
		
	End Function

End Class